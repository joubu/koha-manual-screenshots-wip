package KohaManualScreenshots;
use Mojo::Base 'Mojolicious', -signatures;
use Mojo::SQLite;

use KohaManualScreenshots::Model::Images;
use KohaManualScreenshots::Paths;
use KohaManualScreenshots::Cmd;
use KohaManualScreenshots::Worker;

sub startup ($self) {

    my $config = $self->plugin('NotYAMLConfig');

    $self->secrets( $config->{secrets} );

    $self->helper(
        sqlite => sub { state $sql = Mojo::SQLite->new('sqlite:database.db') }
    );
    $self->helper(
        images => sub {
            state $images = KohaManualScreenshots::Model::Images->new(
                sqlite => shift->sqlite );
        }
    );
    $self->helper(
        specs => sub {
            state $specs = KohaManualScreenshots::Specs->new(
                {
                    path    => $self->config->{paths}->{"cypress-specs"},
                    koha_pl => $self->paths->list_koha_pl
                }
            );
        }
    );
    $self->helper(
        paths => sub {
            state $paths = KohaManualScreenshots::Paths->new(
                { paths => $self->config->{paths}, cmd => $self->cmd } );
        }
    );
    $self->helper(
        cmd => sub {
            state $cmd = KohaManualScreenshots::Cmd->new();
        }
    );
    $self->plugin( Minion => { SQLite => 'sqlite:database.db' } );

    push @{ $self->commands->namespaces }, 'KohaManualScreenshots::Command';

    KohaManualScreenshots::Worker->add_tasks($self);

    my $r = $self->routes;

    $r->get('/')->to('Images#list');
    $r->get('/login')->to('Auth#login_form');
    $r->post('/login')->to('Auth#valid_login');
    $r->any('/logout')->to('Auth#logout');
    $r->get('/image/:name')->to('Images#show');
    $r->get('/images')->to('Images#list_all');

    $r->under(
        sub ($c) {
            return 1 if $c->session('is_auth');
            $c->render( template => 'login', status => 403 );
            return undef;
        }
    );

    $r->get('/image/:name/edit')->to('Images#edit_form');

    $r->put('/image/:name')->to('Images#edit');
}

1;
