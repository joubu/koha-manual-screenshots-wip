package KohaManualScreenshots::Screenshots;

use Mojo::Base -base;

sub generate_screenshot {
    my $self = shift;
    my $app = shift;
    my $name = shift;
    my $image = $app->images->get($name) or return;

    my $specs_path = $app->config->{paths}->{"cypress-specs"};
    ( my $module_path = $image->{abs_filepath}) =~ s|(.*)/\w+$|$1|;
    my $filepath = sprintf("%s/%s.ts", $specs_path, $image->{abs_filepath});
    return unless -e $filepath;

    # Just in case there are some leftover
    my $cmd = q{rm -f mochawesome.json};
    $app->cmd->r($cmd, {do_not_die => 1});

    $cmd = sprintf(q{rm -f cypress/screenshots/%s.ts/*}, $image->{name});
    $app->cmd->r($cmd, {do_not_die => 1});

    $cmd = $self->cypress_cmd( { lang => 'en', spec => $filepath } );
    my $r = $app->cmd->r( $cmd, { do_not_die => 1 } );
    unless ( $r->{error_code} ) {
        $cmd = $self->cypress_cmd( { lang => 'es-ES', spec => $filepath } );
        $app->cmd->r( $cmd, { do_not_die => 1 } );
        $cmd = $self->cypress_cmd( { lang => 'fr-FR', spec => $filepath } );
        $app->cmd->r( $cmd, { do_not_die => 1 } );
    }

    $cmd = sprintf( q{rm -rf public/screenshots/%s.ts/* }, $image->{name} )
      . sprintf( q{&& mkdir -p public/screenshots/%s.ts }, $image->{abs_filepath} )
        # Cannot move - "Permission denied"
      . sprintf(q{&& cp cypress/screenshots/%s.ts/* public/screenshots/%s.ts/}, $image->{name}, $image->{abs_filepath}
      );
    $app->cmd->r( $cmd, { do_not_die => 1 } );

    # Update if the image has a "new" screenshot
    my $screenshot_filepath = sprintf q{screenshots/%s.ts/%s.png},
      $image->{abs_filepath}, $image->{name};
    $app->images->update(
        $image->{name},
        {
            has_new_screenshot => ( -e "public/$screenshot_filepath" ) ? 1 : 0,
            filepath           => ( -e "public/$screenshot_filepath" ) ? $screenshot_filepath : undef,
        },
    );

    $cmd = q{yarn run --silent mochawesome-merge "public/results/*.json" > mochawesome.json};
    $app->cmd->r( $cmd, { do_not_die => 1 } );
    $cmd = q{yarn run marge mochawesome.json && rm mochawesome.json};
    $app->cmd->r( $cmd, { do_not_die => 1 } );
    $cmd = q{cp -r mochawesome-report public/ && rm -rf mochawesome-report };
    $app->cmd->r( $cmd, { do_not_die => 1 } );

    return $r;
}

sub cypress_cmd {
    my $self = shift;
    my $params = shift;
    my $uid = qx{id -u};
    my $gid = qx{id -g};
    chomp for ($uid, $gid);
    return sprintf q{
    docker run -it \
      -v $PWD:/e2e \
      -v /tmp/.X11-unix:/tmp/.X11-unix \
      -w /e2e \
      --user %s:%s \
      --entrypoint cypress \
      --network koha_kohanet \
      cypress/included:13.7.1 run --config video=false,screenshotOnRunFailure=false,trashAssetsBeforeRuns=false --reporter mochawesome --reporter-options 'reportDir=public/results/,reportFilename=[name]-report,overwrite=true,json=true,html=false,toConsole=true' --env KOHA_LANG=%s --project . --spec %s
      }, $uid, $gid, $params->{lang} || 'en', $params->{spec};

}

1;
