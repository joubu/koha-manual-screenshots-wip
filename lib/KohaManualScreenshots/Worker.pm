package KohaManualScreenshots::Worker;

use Mojo::Base -signatures;
use KohaManualScreenshots::Screenshots;

sub add_tasks {
    my $self = shift;
    my $app = shift;
    $app->minion->add_task(generate_screenshot=> sub ($job, @args) {
        my $name = $args[0]->{name};
        $job->app->log->debug("Starting task for " . $name . " job id=" . $job->id);
        my $r = KohaManualScreenshots::Screenshots->new->generate_screenshot($app, $name);
        unless ($r){
            $job->fail('Invalid image name of no spec file exists: ' . $name);
        } elsif ($r->{error_code}){
            $job->fail(sprintf "Failed with %s: %s", $r->{error_code}, $r->{output});
        } else {
            $job->finish('All went well!');
        }
    });
}

1;
