package KohaManualScreenshots::Model::Images;

use Mojo::Base -base;

has 'sqlite';

sub fetch {
    my $self = shift;
    return $self->sqlite->db->select( 'images', undef,
        [ { has_new_screenshot => 1 }, { test_failure => { '!=' => undef } } ] )
      ->hashes->to_array;
}

sub fetch_all {
    my $self = shift;
    return $self->sqlite->db->select('images')->hashes->to_array;
}

sub get {
    my $self = shift;
    my $name = shift;
    return $self->sqlite->db->select( 'images', undef, { name => $name } )
      ->hash;
}

sub update {
    my $self = shift;
    my $name = shift;
    my $info = shift;
    $self->sqlite->db->update( 'images', $info, { name => $name } );
}

1;
