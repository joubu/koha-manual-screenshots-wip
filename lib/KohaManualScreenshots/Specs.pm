package KohaManualScreenshots::Specs;

use Mojo::Base -base, -signatures;

use List::Util qw(first);
use File::Path qw(make_path);
use File::Slurp qw(write_file);

has 'path';
has 'koha_pl';

sub update ( $self, $image ) {

    unless ( $image->{page} ) {
        $self->write_spec( $image, q{} );
    }

    my @steps;
    my $s = $image->{page};
    $s =~ s|\?.*$||;              # currency.pl?op=add_form
    $s =~ s|#.*$||;               # columns_settings.pl#currency
    $s =~ s|^/||;                 # /admin/branches.pl?op=add_form
    $s =~ s|^cgi-bin/koha/||;     #cgi-bin/koha/mainpage.pl
    $s =~ s|^/cgi-bin/koha/||;    #/cgi-bin/koha/mainpage.pl

    my @scripts = @{$self->koha_pl};
    my $page    = first { $_ =~ m{$s} } @scripts;
    if ( $image->{page} =~ m{(\?.*$)} ) {
        $page .= $1;
    }
    if ( $image->{page} =~ m{(#.*$)} ) {
        $page .= $1;
    }

    unless ($page) {
        warn sprintf "'%s' => '%s' not found!", $image->{page}, $page;
        $self->write_spec( $image, q{} );
        next;
    }

    my @sysprefs;
    $page =~ s#^/##;
    push @steps, sprintf( q{cy.visit("/cgi-bin/koha/%s");}, $page );
    for my $step ( split "\n", $image->{workflow} ) {

       #cy.set_syspref("AllowStaffToSetCheckoutsVisibilityForGuarantor", "1");
       #cy.edit_patron(37); // Modify patron Lisa Charles (23529000197047) (Kid)
       #cy.get("#memberentry_guarantor")
       #    .should("be.visible")
       #    .screenshot("allowstafftosetvisibilityforguarantor");
        if ( $step =~ m{^click (.*)} ) {
            push @steps, sprintf( q{cy.get("%s").click();}, $self->escape($1) );
        }
        elsif ( $step =~ m{^go_to (.*)} ) {
            my $s    = $1;
            my $page = first { $_ =~ m{$s} } @scripts;
            warn "Cannot find page $s" unless $page;
            $page =~ s#^/##;
            push @steps, sprintf( q{cy.visit("/cgi-bin/koha/%s");}, $page );
        }
        elsif ( $step =~ m{set_syspref ([^=]+)=(.*)} ) {
            push @sysprefs,
              sprintf( q{cy.set_syspref("%s", "%s");},
                $self->escape($1), $self->escape($2) );
        }
        else {
            push @steps, sprintf "/* FIXME - Invalid step %s */", $step;
        }
    }
    push @steps,
      sprintf(
        q{cy.get("%s").should('be.visible').screenshot("%s");},
        $self->escape( $image->{selector} ),
        $self->escape( $image->{name} )
      );
    my $steps = join "\n        ", ( @sysprefs, @steps );

    my $content = <<EOF;
describe("$image->{abs_filepath}", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("$image->{name}", function () {
        $steps
    });
});
EOF

    $self->write_spec( $image, $content );
}

sub write_spec ( $self, $image, $content ) {

    ( my $module_path = $image->{abs_filepath} ) =~ s|(.*)/\w+$|$1|;
    my $filepath = sprintf( "%s/%s.ts", $self->path, $image->{abs_filepath} );

    unless ($content) {
        unlink $filepath;
        return;
    }

    make_path sprintf( "%s/$module_path", $self->path );
    write_file( $filepath, $content );
}

sub escape ( $self, $s ) {
    $s =~ s{"}{\\"}g;
    return $s;
}

1;
