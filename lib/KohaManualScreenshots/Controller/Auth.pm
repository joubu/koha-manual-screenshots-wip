package KohaManualScreenshots::Controller::Auth;
use Mojo::Base 'Mojolicious::Controller', -signatures;

sub login_form($self){
    $self->render( template => "login" );
}

sub valid_login($self){
    my $username = $self->param('username');
    my $password = $self->param('password');

    my $users = $self->config->{users};
    if ( exists $users->{$username} && $users->{$username} eq $password ) {
        $self->session( is_auth => 1 );
        $self->flash( message => 'You are correctly authenticated!' );
        $self->redirect_to('/');
        return;
    }
    $self->flash( error => 'Incorrect username/password' );
    $self->redirect_to('/login');
}

sub logout($self){
    $self->session( expires => 1 );
    $self->flash( message => 'Logged out!' );
    $self->redirect_to('/login');
}

1;
