package KohaManualScreenshots::Controller::Images;
use Mojo::Base 'Mojolicious::Controller', -signatures;

use KohaManualScreenshots::Specs;
use File::Slurp qw(read_file);

sub list ($self) {
    my $images = $self->images->fetch;
    $self->stash( images => $images );
    $self->render( template => 'index' );
}

sub show ($self) {
    my $name = $self->param('name');
    my $i    = $self->images->get($name);
    unless ($i) {
        $self->stash( image => undef );
        $self->stash( job => undef );
        return $self->render( template => 'image', status => 404 );
    }

    my $spec_filepath = sprintf( '%s/%s.ts',
        $self->config->{paths}->{"cypress-specs"},
        $i->{abs_filepath} );
    if ( -e $spec_filepath ) {
        $i->{spec} = read_file($spec_filepath);
        my @stat = stat($spec_filepath);
        $i->{spec_updated_on} = localtime( $stat[9] );
    }
    if ( $i->{has_new_screenshot} ) {

# If we get a warning from here it means that the DB has not been kept up-to-date (?)
        my @stat = stat( sprintf "public/%s", $i->{filepath} );
        $i->{updated_on} = localtime( $stat[9] );
    }
    my $translated_versions = {};
    for my $lang ( @{ $self->config->{langs} } ) {
        my $filepath = sprintf( 'screenshots/%s.ts/%s.%s.png',
            $i->{abs_filepath}, $i->{name}, $lang );
        my $screenshot_exists = -e sprintf( 'public/%s', $filepath );
        $translated_versions->{$lang} = "/$filepath" if $screenshot_exists;
    }

    $i->{translated_versions} = $translated_versions;

    my @active_jobs = ();
    my $next_jobs   = $self->minion->jobs(
        {
            tasks  => ['generate_screenshot'],
            states => [ 'active', 'inactive' ]
        }
    );
    while ( my $j = $next_jobs->next ) {
        push @active_jobs, $j;
    }

    my ( $first_job, $job );
    for my $j ( sort { $a->{id} <=> $b->{id} } @active_jobs ) {
        $first_job ||= $j;
        if ( $j->{args}->[0]->{name} eq $i->{name} ) {
            $job = $j;
            $job->{total} = $next_jobs->total;

      # Assume jobs are picked in FIFO one by one, might be wrong in the future!
            $job->{position} = $j->{id} - $first_job->{id} + 1;
            last;
        }
    }
    $self->stash( job   => $job );
    $self->stash( image => $i );
    $self->render( template => 'image' );
}

sub edit_form ($self) {
    my $i = $self->images->get( $self->param('name') );
    if ($i) {
        my $spec_filepath = sprintf( '%s/%s.ts',
            $self->config->{paths}->{"cypress-specs"},
            $i->{abs_filepath} );
        if ( -e $spec_filepath ) {
            $i->{spec} = read_file($spec_filepath);
            my @stat = stat($spec_filepath);
            $i->{spec_updated_on} = localtime( $stat[9] );
        }
    }

    $self->stash( image => $i );

    $self->render( template => 'edit_image' );
}

sub edit {
    my $self = shift;
    my $name = $self->param('name');
    my $body = $self->req->json;
    $self->images->update(
        $name,
        {
            ( exists $body->{page} ? ( page => $body->{page} ) : () ),
            (
                exists $body->{selector} ? ( selector => $body->{selector} )
                : ()
            ),
            (
                exists $body->{missing_selector}
                ? ( missing_selector => $body->{missing_selector} )
                : ()
            ),
            (
                exists $body->{workflow} ? ( workflow => $body->{workflow} )
                : ()
            ),
            (
                exists $body->{difficulty}
                ? ( difficulty => $body->{difficulty} )
                : ()
            ),
            ( exists $body->{notes} ? ( notes => $body->{notes} ) : () ),
            (
                exists $body->{additional_ktd_data}
                ? ( additional_ktd_data => $body->{additional_ktd_data} )
                : ()
            ),
        },
    );


    if (   exists $body->{page}
        || exists $body->{selector}
        || exists $body->{workflow} )
    {
        $self->specs->update($self->images->get($name));
        $self->minion->enqueue( generate_screenshot => [ { name => $name } ] );
    }
    $self->render( json => $self->images->get($name) );
}

sub list_all ($self) {
    my $images = $self->images->fetch_all();
    $self->stash( images => $images );
    $self->render( template => "images" );
}

1;
