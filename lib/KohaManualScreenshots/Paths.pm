package KohaManualScreenshots::Paths;

use Mojo::Base -base;

has 'paths';
has 'cmd';

sub list_koha_pl {
    my $self      = shift;
    my $koha_path = $self->paths->{koha};
    my $pl_list = $self->cmd->r( sprintf( q{find %s -name '*.pl'}, $koha_path ),
        { silent => 1 } );
    my @scripts;
    for my $pl ( split "\n", $pl_list->{output} ) {
        next if $pl =~ m{installer/data/mysql/db_revs/};
        next if $pl =~ m{opac};                            # FIXME Support OPAC!
        ( my $script = $pl ) =~ s|$koha_path||;
        push @scripts, $script;
    }
    return \@scripts;
}

1;
