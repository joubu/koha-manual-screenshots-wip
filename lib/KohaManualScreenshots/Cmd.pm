package KohaManualScreenshots::Cmd;

use Mojo::Base -base;
use POSIX qw(strftime);
use IPC::Cmd qw(run);

sub r {
    my $self       = shift;
    my $cmd        = shift;
    my $params     = shift;
    my $silent     = $params->{silent}     || 0;
    my $do_not_die = $params->{do_not_die} || 0;

    say sprintf "[%s]", strftime( "%Y-%m-%d %H:%M:%S", localtime )
      unless $silent;
    my ( $success, $error_code, $full_buf, $stdout_buf, $stderr_buf ) =
      run( command => $cmd, verbose => !$silent );
    die sprintf "Command failed with %s\n", $error_code
      unless $do_not_die || $success;
    my $output = join( '', map { chomp; $_ } @$full_buf );
    return { output => $output, error_code => $error_code };
}

1;
