package KohaManualScreenshots::Command::generate_specs;
use Mojo::Base 'Mojolicious::Command';

has description =>
  'Regenerate Cypress specs from the spec data in the database';
has usage => <<"USAGE";
$0 generate_specs
USAGE

sub run {
    my ( $self, @args ) = @_;
    my $images = $self->app->images->fetch;
    my $total  = scalar @$images;
    my $i;
    for my $image (@$images) {
        say sprintf "Generating spec file for %s (%s/%s)", $image->{name},
          ++$i, $total;
        $self->app->specs->update( $self->app->images->get( $image->{name} ) );
    }
}

1;
