package KohaManualScreenshots::Command::generate_screenshots;
use Mojo::Base 'Mojolicious::Command';

use Mojo::Util 'getopt';

use KohaManualScreenshots::Screenshots;

has description => 'Regenerate screenshots from the spec files';
has usage       => <<"USAGE";
$0 generate_screenshots [image_name] [OPTIONS]
OPTIONS:
  --if-none-exists  Will only generate the screenshot if none exists on the disk already

If no image_name is passed all the screenshots will be generated
USAGE

sub run {
    my ( $self, @args ) = @_;

    getopt( \@args, 'if-none-exists' => \my $missing, );
    my @images = @{ $self->app->images->fetch_all };
    my $total  = scalar @images;
    my $i;
    for my $image (@images) {
        if (
            $missing && -e sprintf q{public/screenshots/%s.ts/%s.png},
            $image->{abs_filepath},
            $image->{name}
          )
        {
            say sprintf "Skipping screenshot for %s (%s/%s) - already exists",
              $image->{name}, ++$i, $total;
            next;
        }
        say sprintf "Generating screenshot for %s (%s/%s)", $image->{name},
          ++$i, $total;
        my $r = KohaManualScreenshots::Screenshots->new->generate_screenshot(
            $self->app, $image->{name} );
        unless ($r) {
            warn 'Invalid image name of no spec file exists: ' . $image->{name};
        }
        elsif ( $r->{error_code} ) {
            warn sprintf "Failed with %s: %s", $r->{error_code}, $r->{output};
        }
    }
}

1;
