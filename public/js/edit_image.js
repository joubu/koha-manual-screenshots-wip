$(document).ready(function(){
    $("#edit_difficulty").on("click", function(e){
        e.preventDefault();
        $("#difficulty").hide();
        $("#new_difficulty").show();
    });
    $("#cancel_edit_difficulty").on("click", function(e){
        e.preventDefault();
        $("#difficulty").show();
        $("#new_difficulty").hide();
    });

    $("#save_difficulty").on("click", async function(e){
        e.preventDefault();
        await fetch("/image/" + image_name, {
            method: "PUT",
            body: JSON.stringify({
                difficulty: $("#new_difficulty option:selected").val(),
            }),
        }).then(response => {
            if ( !response.ok ) {
                throw new Error("Something went wrong!");
            } else {
                return response.json();
            }
        }).then(result => {
            $("#difficulty").html(result.difficulty);
            $("#difficulty").show();
            $("#new_difficulty").hide();
        }).catch( err => alert(err) );
    });


    $("#edit_notes").on("click", function(e){
        e.preventDefault();
        $("#notes").hide();
        $("#new_notes").show();
    });
    $("#cancel_edit_notes").on("click", function(e){
        e.preventDefault();
        $("#notes").show();
        $("#new_notes").hide();
    });

    $("#save_notes").on("click", async function(e){
        e.preventDefault();
        await fetch("/image/" + image_name, {
            method: "PUT",
            body: JSON.stringify({
                notes: $("#new_notes textarea").val(),
            }),
        }).then(response => {
            if ( !response.ok ) {
                throw new Error("Something went wrong!");
            } else {
                return response.json();
            }
        }).then(result => {
            $("#notes").html("<pre>"+result.notes+"</pre>");
            $("#notes").show();
            $("#new_notes").hide();
        }).catch( err => alert(err) );
    });

    $("#edit_additional_ktd_data").on("click", function(e){
        e.preventDefault();
        $("#additional_ktd_data").hide();
        $("#new_additional_ktd_data").show();
    });
    $("#cancel_edit_additional_ktd_data").on("click", function(e){
        e.preventDefault();
        $("#additional_ktd_data").show();
        $("#new_additional_ktd_data").hide();
    });

    $("#save_additional_ktd_data").on("click", async function(e){
        e.preventDefault();
        await fetch("/image/" + image_name, {
            method: "PUT",
            body: JSON.stringify({
                additional_ktd_data: $("#new_additional_ktd_data textarea").val(),
            }),
        }).then(response => {
            if ( !response.ok ) {
                throw new Error("Something went wrong!");
            } else {
                return response.json();
            }
        }).then(result => {
            $("#additional_ktd_data").html("<pre>"+result.additional_ktd_data+"</pre>");
            $("#additional_ktd_data").show();
            $("#new_additional_ktd_data").hide();
        }).catch( err => alert(err) );
    });

});
