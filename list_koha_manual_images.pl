use Modern::Perl;
use File::Slurp qw(read_file);

# We require unicity on image names and do not process if duplicated
my ( $existing_images, @ignore );
for my $i (read_file('koha_manual_images_in.txt')){
    chomp $i;
    #source/images/about/aboutperlmodules.png
    $i =~ s#source/images/##;
    if ( $i =~ m{^(.+)/([^/\.]+)\.png$} ) { # Skip non-png images
        my $module = $1;
        my $id = $2;
        my $ext = $3;
        if ( exists $existing_images->{$id} ) {
            warn "$i already exists, removing\n";
            push @ignore, $id;
            delete $existing_images->{$i};
            next;
        }
        next if grep {$_ eq $id} @ignore;

        $existing_images->{$id} = "$module/$id";
    } else {
        warn "$i does not match the expected format\n";
    }
}
# Reverse so we can sort on values
my %reverse = reverse %$existing_images;
say sprintf('%s:%s', $reverse{$_}, $_) for sort keys %reverse;

