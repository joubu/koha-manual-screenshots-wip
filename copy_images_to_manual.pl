use Modern::Perl;

use File::Slurp qw(read_file);

my $koha_manual_root_dir = "/home/jonathan/workspace/koha-tools/kohadocs";
my $screenshots;
my @dirs = qx{find public/screenshots -type d -name *.ts};
for my $dir ( @dirs ) {
    chomp $dir;
    if ( $dir =~ m{.*/(\w+).ts} ) {
        my $id = $1;
        $screenshots->{$id}->{from_path} = $dir;
    } else {
        warn "$dir does not match the expected dir format";
    }
}
for my $i (read_file('koha_manual_images_out.txt')){
    chomp $i;
    my ( $id, $path ) = split ':', $i;
    ( $screenshots->{$id}->{to_dirpath} = $path ) =~ s|(.*)/\w+$|$1|;
}

for my $id ( sort keys %$screenshots ) {
    next if !$screenshots->{$id}->{from_path} || !$screenshots->{$id}->{to_dirpath};

    my $copy_cmd = sprintf "cp %s/*.png %s/source/images/%s", $screenshots->{$id}->{from_path}, $koha_manual_root_dir, $screenshots->{$id}->{to_dirpath};
    qx{$copy_cmd};
}
