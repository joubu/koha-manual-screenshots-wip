use Modern::Perl;
use Text::CSV::Encoded;
use File::Slurp qw( read_file );
use JSON qw(from_json);
use DBI;

my $dbh = DBI->connect("dbi:SQLite:dbname=database.db", "", "");
$dbh->do(q{DROP TABLE images});
$dbh->do(q{
    CREATE TABLE images (
        id integer primary key autoincrement,
        name text UNIQUE,
        module text,
        filepath text,
        abs_filepath text,
        has_new_screenshot boolean,
        old_url text,
        difficulty text,
        `page` text,
        selector text,
        missing_selector text,
        workflow text,
        notes text,
        done_by text,
        additional_ktd_data text,
        test_failure text
    );
});

my $screenshots_dir = q{../koha-manual-screenshots};

my $list_of_images = $screenshots_dir . "/list_of_images.csv";

my $existing_images;
for my $i (read_file('koha_manual_images_out.txt')){
    chomp $i;
    my ($id, $path) = split ':', $i;
    $existing_images->{$id} = $path;
}

my $test_result = from_json(read_file('mochawesome.json'));
my $failed_tests;
for my $r (@{$test_result->{results}}){
    my $suite = $r->{suites}->[0];
    next unless @{$suite->{failures}};
    my $t = $suite->{tests}->[0];
    my $id = $t->{title};
    my $err = $t->{err}->{estack};
    $err =~ s|\\n|\n|g;
    $failed_tests->{$id} = $err;
}

my $csv = Text::CSV::Encoded->new(
    {
        encoding_out => 'utf8',
        encoding_in  => 'utf8',
        binary       => 1,
        quote_char   => q{"},
        sep_char     => ',',
    }
);


my $sth = $dbh->prepare(
    q{
    INSERT INTO images(name, module, filepath, abs_filepath, has_new_screenshot, old_url, difficulty, page, selector, missing_selector, workflow, notes, done_by, additional_ktd_data, test_failure )
    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
    }
);
my @images;
open (my $in,  $list_of_images );
while( my $columns = $csv->getline( $in ) ) {
    my ( $filename, $module, $image_url, $new_image_url, $difficulty, $page_on_koha, $selector, $missing_selector, $workflow, $notes, $done_by, $ktd_data ) = @$columns;

    unless ( exists $existing_images->{$filename} ) {
        warn sprintf "Images '%s' not in koha_manual_images_out.txt: list of images not up-to-date or has been skipped previously (duplicated or incorrect format)", $filename;
        next;
    }

    my $abs_filepath = $existing_images->{$filename}; # Without extension
    # Do not list if no screenshot generated and no error
    my $filepath = sprintf('screenshots/%s.ts/%s.png', $abs_filepath, $filename );
    my $screenshot_exists = -e sprintf('public/%s', $filepath);
    undef $filepath unless $screenshot_exists;
    #next if !$screenshot_exists && !exists $failed_tests->{$filename};

    my $old = sprintf('https://gitlab.com/koha-community/koha-manual/-/raw/main/source/images/%s.png', $abs_filepath);

    $sth->execute($filename, $module, $filepath, $abs_filepath, $screenshot_exists, $old, $difficulty, $page_on_koha, $selector, $missing_selector, $workflow, $notes, $done_by, $ktd_data, $failed_tests->{$filename});
}
close($in);
